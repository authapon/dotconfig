# Put system-wide fish configuration entries here
# or in .fish files in conf.d/
# Files in conf.d can be overridden by the user
# by files with the same name in $XDG_CONFIG_HOME/fish/conf.d

# This file is run by all fish instances.
# To include configuration only for login shells, use
# if status --is-login
#    ...
# end
# To include configuration only for interactive shells, use
# if status --is-interactive
#   ...
# end

if status --is-interactive
	set fish_greeting	
	export EDITOR=vim
	export STARSHIP_CONFIG=/etc/starship.toml
	
	echo
	neofetch
	alias vpn_surya="sshuttle -r authapon@203.158.177.14 -x 203.158.177.14 0.0.0.0/0 -vvv --dns"
	alias vpn_jupiter="sshuttle -r authapon@203.158.177.196:222 -x 203.158.177.196 0.0.0.0/0 -vvv --dns"
	alias vpn_uranus="sshuttle -r authapon@203.158.180.195:222 -x 203.158.180.195 0.0.0.0/0 -vvv --dns"
	
# uses powerline
#	set fish_function_path $fish_function_path "/usr/lib/python3.9/site-packages/powerline/bindings/fish"
#	powerline-setup

# uses starship
	starship init fish | source

# start X11 automatically if it is on console 1
	if test -z $DISPLAY && ! test -z $XDG_VTNR && test $XDG_VTNR -eq 1
	#	exec startx
		exec sway
	end
end

